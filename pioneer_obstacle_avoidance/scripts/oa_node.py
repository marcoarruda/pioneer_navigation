#!/usr/bin/env python
import rospy # basic ROS functionalities in python
from sensor_msgs.msg import Range
from webots_ros.srv import motor_set_velocity

# ###### ######### #
# global variables #
# ###### ######### #
_controllerName = None # name of the robot in simulator
_so2_range = 0 # sonar 2
_so3_range = 0 # sonar 3
_so4_range = 0 # sonar 4
_so5_range = 0 # sonar 5
_left_wheel_speed_service = None # left wheel service
_rght_wheel_speed_service = None # right wheel service

# ######### #
# callbacks #
# ######### #
def _callback_shutdown():
    global _left_wheel_speed_service
    _left_wheel_speed_service(0)
    global _rght_wheel_speed_service
    _rght_wheel_speed_service(0)

def _callback_so0(data):
    global _so0_range
    _so0_range = data.range

def _callback_so1(data):
    global _so1_range
    _so1_range = data.range

def _callback_so2(data):
    global _so2_range
    _so2_range = data.range

def _callback_so3(data):
    global _so3_range
    _so3_range = data.range

def _callback_so4(data):
    global _so4_range
    _so4_range = data.range

def _callback_so5(data):
    global _so5_range
    _so5_range = data.range

def _callback_so6(data):
    global _so6_range
    _so6_range = data.range

def _callback_so7(data):
    global _so7_range
    _so7_range = data.range

# #### ####### #
# main program #
# #### ####### #
def main():
    # initialize node
    rospy.init_node('obstacle_avoidance', anonymous=True)

    # set a callback for shutdown event
    rospy.on_shutdown(_callback_shutdown)
    
    # define a rate
    rate = rospy.Rate(10)
    
    # get the name of the robot, which is a prefix for topics and services naming
    global _controllerName
    while not rospy.has_param('/p3dx_name'):
        rospy.loginfo('waiting for param..')
        rospy.sleep(0.1)
    _controllerName = rospy.get_param('/p3dx_name')
    rospy.loginfo('param\'s ready!')
    
    rospy.wait_for_service(_controllerName + '/right_wheel/set_velocity')
    rospy.wait_for_service(_controllerName + '/left_wheel/set_velocity')
    rospy.loginfo('Wheel services are ready!')
    
    # subscribe ultrasonic sensors
    _sub_so0 = rospy.Subscriber(_controllerName + '/so0/value', Range, _callback_so0)
    _sub_so1 = rospy.Subscriber(_controllerName + '/so1/value', Range, _callback_so1)
    _sub_so2 = rospy.Subscriber(_controllerName + '/so2/value', Range, _callback_so2)
    _sub_so3 = rospy.Subscriber(_controllerName + '/so3/value', Range, _callback_so3)
    _sub_so4 = rospy.Subscriber(_controllerName + '/so4/value', Range, _callback_so4)
    _sub_so5 = rospy.Subscriber(_controllerName + '/so5/value', Range, _callback_so5)
    _sub_so6 = rospy.Subscriber(_controllerName + '/so6/value', Range, _callback_so6)
    _sub_so7 = rospy.Subscriber(_controllerName + '/so7/value', Range, _callback_so7)
    
    # velocity services
    global _left_wheel_speed_service
    _left_wheel_speed_service = rospy.ServiceProxy(_controllerName + '/left_wheel/set_velocity', motor_set_velocity)
    global _rght_wheel_speed_service
    _rght_wheel_speed_service = rospy.ServiceProxy(_controllerName + '/right_wheel/set_velocity', motor_set_velocity)
    
    # working loop
    while not rospy.is_shutdown():
        val = min(_so2_range, _so3_range, _so4_range, _so5_range)
        if val < 0.3:
            _left_wheel_speed_service(-2)
            _rght_wheel_speed_service(-2)
        elif val >= 0.3 and val < 0.7:
            if min(_so0_range, _so1_range) < min(_so6_range, _so7_range):
                _left_wheel_speed_service(2)
                _rght_wheel_speed_service(0)
            else:
                _left_wheel_speed_service(0)
                _rght_wheel_speed_service(2)
        else:
            _left_wheel_speed_service(2)
            _rght_wheel_speed_service(2)
        rate.sleep()

# python stuff to call main function
if __name__ == '__main__':
    try:
        main()
    except rospy.ROSInterruptException:
        pass
